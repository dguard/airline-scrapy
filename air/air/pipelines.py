from scrapy.exceptions import DropItem


class ArrivalTimePipeline(object):

    def process_item(self, item, spider):
        if spider.arrival_time and spider.arrival_time != str(item['arrival_time']):
            raise DropItem("arrival_time does not compare for item: %s" % item)
        else:
            return item


class DepartureTimePipeline(object):

    def process_item(self, item, spider):
        if spider.departure_time and spider.departure_time != str(item['departure_time']):
            raise DropItem("departure_time does not compare for item: %s" % item)
        else:
            return item


class DuplicatesPipeline(object):

    def __init__(self):
        self.ids_seen = set()

    def process_item(self, item, spider):
        _id = item['arrival_time'] + item['departure_time'] + str(item['price'])
        if _id in self.ids_seen:
            raise DropItem("Duplicate item found: %s" % item)
        else:
            self.ids_seen.add(_id)
            return item


class AddExportTimePipeline(object):

    def process_item(self, item, spider):
        item['export_time'] = spider.export_time
        return item