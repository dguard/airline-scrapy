# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# http://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class AirItem(scrapy.Item):
    export_time = scrapy.Field()
    price = scrapy.Field()
    arrival_time = scrapy.Field()
    arrival_city_name = scrapy.Field()
    departure_city_name = scrapy.Field()
    departure_time = scrapy.Field()