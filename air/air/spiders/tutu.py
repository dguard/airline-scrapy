from scrapy.spider import Spider
from air.items import AirItem
from scrapy.http import Request
import json
import time
from scrapy.exceptions import CloseSpider


class JsonObj(object):
    """ just a wrapper for string with read method """
    def __init__(self, _str):
        self.str = _str

    def read(self):
        return self.str


class TutuSpider(Spider):
    name = 'tutu'
    allowed_domains = ["avia.tutu.ru"]
    start_urls = [
        'http://avia.tutu.ru/ajax/?Action=avia_offer&act=get_offers&force=0&class=Y&changes=all&passengers=100&route%5B%5D=29-12122014-75'
    ]

    def __init__(self, url=None, arrival_time=None, departure_time=None, *args, **kwargs):
        super(TutuSpider, self).__init__(*args, **kwargs)
        self.export_time = time.strftime("%Y-%m-%d %H:%M:%S")
        if url:
            self.start_urls = [url]
        self.arrival_time = arrival_time
        self.departure_time = departure_time

    def parse(self, response):
        if 'requested' in response.body:
            time.sleep(5)
            url = response.url + '&_=%s' % time.time().__trunc__()  # don't cache
            return Request(url, callback=self.parse)
        elif 'error' in response.body:
            raise CloseSpider('Invalid Request. Body output is: %s' % response.body)
        else:
            return self.parse_item(response)

    def parse_item(self, response):
        resp_json = json.load(JsonObj(response.body))

        for list_item in resp_json.get('list'):
            for raw_item in list_item:
                item = AirItem()
                item['price'] = raw_item['price']
                segment = raw_item.get('routes').itervalues().next().get('segments')[0]
                fields = ['arrival_time', 'arrival_city_name', 'departure_time', 'departure_city_name']

                for field in fields:
                    item[field] = segment[field]
                yield item