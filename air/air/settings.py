# -*- coding: utf-8 -*-

# Scrapy settings for air project
#
# For simplicity, this file contains only the most important settings by
# default. All the other settings are documented here:
#
#     http://doc.scrapy.org/en/latest/topics/settings.html
#

BOT_NAME = 'air'

SPIDER_MODULES = ['air.spiders']
NEWSPIDER_MODULE = 'air.spiders'

# Crawl responsibly by identifying yourself (and your website) on the user-agent
#USER_AGENT = 'air (+http://www.yourdomain.com)'

ITEM_PIPELINES = {
    'air.pipelines.ArrivalTimePipeline': 100,
    'air.pipelines.DepartureTimePipeline': 200,
    'air.pipelines.DuplicatesPipeline': 300,
    'air.pipelines.AddExportTimePipeline': 400,
}

# By specifying the fields to export, the CSV export honors the order
# rather than using a random order.
EXPORT_FIELDS = [
    'export_time',
    'departure_city_name',
    'arrival_city_name',
    'departure_time',
    'arrival_time',
    'price',
]

FEED_EXPORTERS = {
    'csv': 'air.feedexport.CSVkwItemExporter'
}