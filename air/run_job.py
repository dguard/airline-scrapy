import subprocess
import time
import csv
import re
from os.path import dirname
import os

ROOT_DIR = dirname(os.path.abspath(__file__))

with open(os.path.join(ROOT_DIR, 'job.csv'), 'rb') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    spamreader.next()

    command = ' '.join([
        '{scrapy} crawl tutu',
        '-o {filename}.csv -t csv',
        '-a url="http://avia.tutu.ru/ajax/?Action=avia_offer&act=get_offers&force=0&class=Y&changes=all&passengers=100&route%5B%5D={route}"',
        '-a departure_time="{departure_time}"',
        '-a arrival_time="{arrival_time}"',
    ])
    os.chdir(ROOT_DIR)
    scrapy = os.path.join(ROOT_DIR, '..', '..', 'env', 'bin', 'scrapy')

    for row in spamreader:
        route, departure_time, arrival_time, comment = row

        filename = "%s__%s__%s" % (departure_time, arrival_time, route)
        filename = re.sub('[\s:]', '_', filename)
        filename = os.path.join('results', filename)

        current_command = ''.join(command).format(
            scrapy=scrapy,
            route=route,
            arrival_time=arrival_time,
            departure_time=departure_time,
            filename=filename
        )
        result = subprocess.Popen(current_command, shell=True, stdout=subprocess.PIPE)

        while True:
            line = result.stdout.readline()
            print line.strip()
            if not line:
                break
        time.sleep(5)
